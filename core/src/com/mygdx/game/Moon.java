package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

class Moon extends Body {
    float x, y;
    Texture planetTexture;

    Moon (float positionX, float positionY, Texture texture) {
        super(positionX, positionY, texture);

        x = positionX;
        y = positionY;
        planetTexture = texture;
    }
}