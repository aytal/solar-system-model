package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.TimeUtils;

public class MyGdxGame extends ApplicationAdapter {
    private SpriteBatch batch;
    private Texture background, sunTexture;
    private Sprite backgroundSprite;
    private float windowWidth, windowHeight; // Ширина и высота окна
    private Venus venus;
    private Earth earth;
    private Moon moon;
    private Stage stage;
    private long initialTime;
    private boolean play;

	@Override
	public void create () {
        // высота и ширина окна
        windowWidth = Gdx.graphics.getWidth();
        windowHeight = Gdx.graphics.getHeight();

        //время нужно для подсчета скорости
        initialTime = TimeUtils.nanoTime();

        // создание SpriteBatch
		batch = new SpriteBatch();

		// создание фона
        background = new Texture("space.png");
        backgroundSprite = new Sprite(background);
        backgroundSprite.setSize(windowWidth, windowHeight);

        sunTexture = new Texture("sun.png");

        // создание обьекта Венера
        venus = new Venus(windowWidth / 2 - 32,
                windowHeight / 2 + 128,
                new Texture("venus.png"));

        // создание обьекта Земля
        earth = new Earth(windowWidth / 2 - 32,
                venus.y + 128,
                new Texture("earth.png"));

        // создание обьекта Луна
        moon = new Moon(windowWidth / 2 - 16,
                earth.y + 80,
                new Texture("moon.png"));

        // создание кнопки
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        BitmapFont font = new BitmapFont();
        Skin skin = new Skin();
        TextureAtlas buttonAtlas = new TextureAtlas(Gdx.files.internal("buttons.atlas"));
        skin.addRegions(buttonAtlas);
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = font;
        textButtonStyle.up = skin.getDrawable("pause");
        textButtonStyle.checked = skin.getDrawable("play");
        TextButton button = new TextButton("", textButtonStyle);
        button.setHeight(64);
        button.setWidth(64);
        button.setPosition(windowWidth / 2 - 32, 32);
        stage.addActor(button);

        // обработка нажатия кнопки
        button.addListener(new ChangeListener() {
            @Override
            public void changed (ChangeEvent event, Actor actor) {
                play = !play;
            }
        });

        // Флажок для паузы
        play = true;
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();

        if (play) {
            float elapsedNanoseconds = TimeUtils.nanoTime() - initialTime;
            float elapsedSeconds = MathUtils.nanoToSec * elapsedNanoseconds;

            venus.updateCoords(windowWidth / 2 - 32, windowHeight / 2 - 32, 128, elapsedSeconds, 4);
            earth.updateCoords(windowWidth / 2 - 32, windowHeight / 2 - 32, 256, elapsedSeconds, 8);
            moon.updateCoords(earth.getX() + 16, earth.getY() + 16, 64, elapsedSeconds,  6);
        }

		backgroundSprite.draw(batch);

		batch.draw(sunTexture, windowWidth / 2 - 64, windowHeight / 2 - 64);
		batch.draw(venus.planetTexture, venus.getX(), venus.getY());
        batch.draw(earth.planetTexture, earth.getX(), earth.getY());
        batch.draw(moon.planetTexture, moon.getX(), moon.getY());

		batch.end();

        stage.draw();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
        background.dispose();
        sunTexture.dispose();
        stage.dispose();
	}
}
