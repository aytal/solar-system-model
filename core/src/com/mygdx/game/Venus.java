package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

class Venus extends Body {
    float x, y;
    Texture planetTexture;

    Venus (float positionX, float positionY, Texture texture) {
        super(positionX, positionY, texture);

        x = positionX;
        y = positionY;
        planetTexture = texture;
    }
}