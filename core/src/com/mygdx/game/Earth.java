package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;

class Earth extends Body {
    float x, y;
    Texture planetTexture;

    Earth (float positionX, float positionY, Texture texture) {
        super(positionX, positionY, texture);

        x = positionX;
        y = positionY;
        planetTexture = texture;
    }
}