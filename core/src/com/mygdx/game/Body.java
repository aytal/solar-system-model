package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;

class Body {
    float x, y;
    Texture planetTexture;

    Body (float positionX, float positionY, Texture texture) {
        x = positionX;
        y = positionY;
        planetTexture = texture;
    }

    void updateCoords (float centerX, float centerY, float r, float elapsedSeconds, int speedRatio) {
        float venusElapsedPeriods = elapsedSeconds / speedRatio;
        float venusCyclePosition = venusElapsedPeriods % 1;

        x = (float) (centerX + r * Math.cos(MathUtils.PI2 * venusCyclePosition));
        y = (float) (centerY + r * Math.sin(MathUtils.PI2 * venusCyclePosition));
    }

    float getX() {
        return x;
    }

    float getY() {
        return y;
    }
}